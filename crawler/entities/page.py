from crawler.entities.entitybase import EntityBase


class Page(EntityBase):

    def __init__(self, data=None):
        self.url = ''
        self.pages = []
        self.css_links = []
        self.script_links = []
        self.image_links = []
        EntityBase.__init__(self, data)

    def tostring(self, depth=0):

        space = ' ' * depth
        print('{0}|URL: {1}'.format(space, self.url))

        space = ' ' * (depth + 5)
        for image in self.image_links:
            print('{0}-- image: {1}'.format(space, image))
        for css in self.css_links:
            print('{0}-- stylesheet: {1}'.format(space, css))
        for script in self.script_links:
            print('{0}-- scripts: {1}'.format(space, script))
        for _page in self.pages:
            if _page is not None:
                _page.tostring(depth + 1)

    @staticmethod
    def to_json(node):

        res = dict(url=node.url, css_links=node.css_links,
                   script_links=node.script_links, image_links=node.image_links,
                   pages=[])
        for node in node.pages:
            if node is not None:
                res['pages'].append(Page.to_json(node))

        return res
