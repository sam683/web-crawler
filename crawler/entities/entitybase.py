import time


class EntityBase(object):
    def __init__(self, data=None):
        self.created_at = time.time()
        self.update_from_dic(data)

    def update_from_dic(self, dic):
        if dic:
            for attr in dic:
                if hasattr(self, attr):
                    setattr(self, attr, dic[attr])
