import asyncio
import requests
from bs4 import BeautifulSoup
from aiohttp import ClientSession
from urllib.parse import urlparse
from crawler.entities.page import Page
from crawler.common.exceptions import YoYoException
from crawler.common.validation_messages import Message
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError



class YoYoCrawler:

    def __init__(self):
        self.baseURL = ''
        self.visited_urls = []

    def start(self, url='https://www.yoyowallet.com', depth=1):

        # validation
        if depth <= 0:
            raise YoYoException(Message.WrongCrawlingDepth.format(depth))

        val = URLValidator()
        try:
            val(url)
        except ValidationError:
            raise YoYoException(Message.InvalidUrlFormat.format(url))

        self.baseURL = url
        self.visited_urls = []

        try:
            response = requests.get(url)
        except YoYoException as ex:
            ex.errors.append(Message.UnableToDownload.format(url))
            raise ex

        if response.content is None:
            raise YoYoException(Message.UnableToDownload.format(url))
        elif response.status_code is not 200:
            raise YoYoException(Message.DownloadFailed.format(response.status_code))

        else:
            content = response.content
            page = self._crawl(url, content, depth)
            return page

    def _crawl(self, url, content, depth=1):

        if content is None:
            raise YoYoException(Message.EmptyContent.format(url))

        base_url_parts = urlparse(url)
        soup = BeautifulSoup(content, 'html.parser')
        html_links = [link for link in YoYoCrawler.extract_internal_links(base_url_parts, soup) if
                      link not in self.visited_urls]

        page = Page()
        page.url = url
        page.image_links = self.extract_images(soup)
        page.script_links = self.extract_scripts(soup)
        page.css_links = self.extract_stylesheets(soup)

        if len(html_links) == 0 or depth <= 0:
            return page

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        future = asyncio.ensure_future(YoYoCrawler._download_all(html_links))
        responses = loop.run_until_complete(future)

        for response in responses:
            if response['url'] not in self.visited_urls:
                self.visited_urls.append(response['url'])

            if response['content'] is None or response['status'] is not 200:
                continue

            child = self._crawl(response['url'], response['content'], depth - 1)
            page.pages.append(child)

        return page

    @staticmethod
    async def _download(url, session):
        async with session.get(url) as response:
            content = await response.read()
            return dict(url=url, content=content, status=response.status)

    @staticmethod
    async def _download_all(url_list):
        tasks = []
        async with ClientSession() as session:
            for i in range(len(url_list)):
                task = asyncio.ensure_future(YoYoCrawler._download(url_list[i], session))
                tasks.append(task)

            responses = await asyncio.gather(*tasks)
            return responses

    @staticmethod
    def extract_internal_links(base_url_parts, soup):

        links = soup.find_all('a', href=True)
        html_links = set([l['href'] for l in links if l['href'].strip().endswith(tuple(['htm', 'html']))])
        urls = []

        for _ in html_links:
            parsed_url = urlparse(_)
            if parsed_url.netloc == '':
                slash = '' if base_url_parts.netloc.endswith('/') or _.startswith('/') else '/'
                urls.append('{0}://{1}{2}{3}'.format(base_url_parts.scheme, base_url_parts.netloc, slash, _))

            # sub domains
            elif base_url_parts.netloc == parsed_url.netloc or \
                    (not base_url_parts.netloc == parsed_url.netloc
                     and parsed_url.netloc.endswith(base_url_parts.netloc)):
                urls.append(_)

        return urls

    @staticmethod
    def extract_images(soup):
        images = soup.find_all('img')
        _set = set([img['src'] for img in images if img.has_attr('src')])
        return [css for css in iter(_set)]

    @staticmethod
    def extract_stylesheets(soup):
        stylesheets = soup.find_all('link', rel='stylesheet')
        _set = set([css['href'] for css in stylesheets if css.has_attr('href')])
        return [css for css in iter(_set)]

    @staticmethod
    def extract_scripts(soup):
        scripts = soup.find_all('script', type="text/javascript")
        _set = set([script['src'] for script in scripts if script.has_attr('src')])
        return [css for css in iter(_set)]


if __name__ == '__main__':
    crawler = YoYoCrawler()
    root = crawler.start(url='https://yoyowallet.com', depth=1)
    root.tostring()