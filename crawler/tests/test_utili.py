def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code, content):
            self.json_data = json_data
            self.status_code = status_code
            self.content = content

        def json(self):
            return self.json_data

    if args[0] == 'http://www.yoyowallet.com':
        return MockResponse({"key1": "value1"}, 200, '<html></html>')

    if args[0] == 'http://www.yoyowallet.com/empty':
        return MockResponse({}, 200, None)

    if args[0] == 'http://www.yoyowallet.com/status':
        return MockResponse({}, 403, '<html></html>')

    return MockResponse(None, 404)


def mock_download_all(html_links):
    return []

