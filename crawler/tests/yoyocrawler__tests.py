from crawler.common.validation_messages import Message
from crawler.common.exceptions import YoYoException
from crawler.yoyocrawler import YoYoCrawler
from crawler.tests.test_utili import *
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from asyncio import coroutine
from unittest import mock
import unittest



class TestYoYoCrawler(unittest.TestCase):

    def setUp(self):
        self._sut = YoYoCrawler()
        self._mockContent = '<html lang="en"><head><link rel="apple-touch-icon" sizes="152x152" ' \
                            'href="/assets/v2/apple-icon-152x152.png"><link rel="icon" type="image/png" ' \
                            'sizes="192x192" href="/assets/v2/android-icon-192x192.png"> <link ' \
                            'href="/assets/v2/favicon.ico" rel="shortcut icon"><li><a ' \
                            'href="http://www.twitter.com/yoyowallet"><img ' \
                            'src="/assets/v2/images/twitter-icon.svg"/></a></li><li><a ' \
                            'href="http://www.facebook.com/yoyowallet"><img ' \
                            'src="/assets/v2/images/facebook-icon.svg"/></a><a href="/index.html" /></a></li><li><a ' \
                            'href="https://www.linkedin.com/company/yoyo-wallet"><img ' \
                            'src="/assets/v2/images/linkedin-icon.svg"/></a></li><link ' \
                            'href="/assets/v2/apple-touch-icon.png" rel="apple-touch-icon"><script ' \
                            'type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax' \
                            '.min.js"></script><script ' \
                            'type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.0.1/plugins' \
                            '/ScrollToPlugin.min.js ' \
                            '"></script></body></html> '

        self._mockNullContent = ''

    def assertRaisesWithMessage(self, msg, func, exception_type, *args, **kwargs):
        try:
            func(*args, **kwargs)
            self.assertFail()
        except exception_type as inst:
            self.assertEqual(inst.message, msg)

    def test_extracting_script_should_return_2_results(self):
        soup = BeautifulSoup(self._mockContent, 'html.parser')
        self.assertEqual(2, len(YoYoCrawler.extract_scripts(soup)))

    def test_extracting_images_should_return_3_results(self):
        soup = BeautifulSoup(self._mockContent, 'html.parser')
        self.assertEqual(3, len(YoYoCrawler.extract_images(soup)))

    def test_extracting_stylesheet_should_return_no_results(self):
        soup = BeautifulSoup(self._mockContent, 'html.parser')
        self.assertEqual(0, len(YoYoCrawler.extract_stylesheets(soup)))

    def test_extracting_extract_internal_links_should_return_1_results(self):
        base_url = urlparse('http://www.yoyowallet.com')
        soup = BeautifulSoup(self._mockContent, 'html.parser')
        self.assertEqual(1, len(YoYoCrawler.extract_internal_links(base_url, soup)))

    def test_fail_whe_I_start_with_negative_crawling_depth(self):
        url = 'http://www.yoyowallet.com'
        depth = -1
        message = Message.WrongCrawlingDepth.format(depth)
        self.assertRaisesWithMessage(message, lambda: self._sut.start(url=url, depth=depth), YoYoException)

    def test_fail_whe_I_start_with_incorrect_url(self):
        url = 'http://yoyowallet'
        message = Message.InvalidUrlFormat.format(url)
        self.assertRaisesWithMessage(message, lambda: self._sut.start(url=url, depth=1), YoYoException)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_succeed_whe_I_start_and_downloaded_website_with_correct_request(self, mock_get):
        url = 'http://www.yoyowallet.com'
        page = self._sut.start(url)
        self.assertIsNotNone(page)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_fail_whe_I_start_and_downloaded_website_is_empty(self, mock_get):
        url = 'http://www.yoyowallet.com/empty'
        message = Message.UnableToDownload.format(url)
        self.assertRaisesWithMessage(message, lambda: self._sut.start(url=url, depth=1), YoYoException)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_fail_whe_I_start_and_downloaded_website_and_status_code_was_not_200(self, mock_get):
        url = 'http://www.yoyowallet.com/status'
        message = Message.DownloadFailed.format(403)
        self.assertRaisesWithMessage(message, lambda: self._sut.start(url=url, depth=1), YoYoException)

    def test_succeed_whe_I_crawl_a_page_with_children(self):
        url = 'http://www.yoyowallet.com'
        content = '<html><a href="/index.html" /></html>'
        YoYoCrawler._download_all = coroutine(mock_download_all)
        page = self._sut._crawl(url=url, content=content,  depth=1)


if __name__ == '__main__':
    unittest.main()
