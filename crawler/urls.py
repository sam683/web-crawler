from django.urls import re_path
from rest_framework.views import APIView
from django.http import JsonResponse
from crawler.yoyocrawler import YoYoCrawler
from crawler.common.exceptions import YoYoException
from crawler.common.validation_messages import Message
from crawler.entities.page import Page



class customView(APIView):
    def get(self, request):

        try:
            url = request.query_params.get('url')
            crawling_depth = int(request.query_params.get('crawling_depth'))
            root = YoYoCrawler().start(url=url, depth=crawling_depth)
            return JsonResponse({'status': True, 'data': Page.to_json(root)})

        except YoYoException as ex:
            return JsonResponse({'status': False, 'message': ','.join(ex.args)}, status=500)
        except Exception:
            return JsonResponse({'status': False, 'message': Message.GeneralServerFail}, status=500)


urlpatterns = [
    re_path(r'^$', customView.as_view()),
]