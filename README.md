## YOYO web crawler
Multi threaded web crawler in Django app using python 3.6


### Description:
Yoyo web crawler is a Django rest service that accepts a url as root page and crawls its nodes based on requested depth of
crawling and in return, outputs a json file. the output data structure is shown below:

```
	{  
   "status":true,
   "data":{  
      "url":"https://www.yoyowallet.com",
      "css_links":[  
         "/assets/v2/css/app.css",
         "//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css"
      ],
      "script_links":[  
         "//js.hsforms.net/forms/v2.js",
         "//js.hs-scripts.com/3484737.js"
      ],
      "image_links":[  
         "/assets/v2/images/screen-yoyo-apps-2x.png",
         "/assets/v2/images/linkedin-icon.svg",
		 ...
      ],
      "pages":[  
		...
      ]
   }
}
```

### API specification

Address: (default Django port - 8000)
```
[GET] http://127.0.0.1:8000/crawl/
```

Input: input data is a an encoded querystring which holds two parametrers: url and crawling_depth
```
parameters: {'url': 'https://www.yoyowallet.com', 'crawling_depth': 1}

example: http://127.0.0.1:8000/crawl/?url=https%3A%2F%2Fwww.yoyowallet.com&crawling_depth=1

```


### install
```
	python version 3.6
	
    virtualenv env/
    source env/bin/activate
    pip install -r requirements.txt
    python manage.py runserver

```

### test
```
python test_crawler.py
```

